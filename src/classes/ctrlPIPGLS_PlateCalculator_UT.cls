/*******************************************************************************
Description:  Test Class for controller ctrlPIPGLS_PlateCalculator and 
                trigger handler PIPGLS_PlateCalculatorHandler
 ********************************************************************************/
@isTest(SeeAllData = true)  
private class ctrlPIPGLS_PlateCalculator_UT {

    static testMethod void ctrlPIPGLS_PlateCalculator_Method1() {

        LIST<PIP_GLS_Plate_Calculator__c> PCK=new LIST<PIP_GLS_Plate_Calculator__c>{
            new PIP_GLS_Plate_Calculator__c(Name='Test1',PIP_GLS_Interlayer_Material__C='SentryGlas®',PIP_GLS_Length_Range__c='500-1000',PIP_GLS_Length__c='500',PIP_GLS_Width_Range__c='500-1000',PIP_GLS_Width__c='500',PIP_GLS_Pressure_Range__c='0-2',PIP_GLS_Pressure__c='0.25',PIP_GLS_Glass_Thickness__c='6',PIP_GLS_Interlayer_Thickness__c='1.52',PIP_GLS_Temperature__c='50',PIP_GLS_Load_Duration__c='3',PIP_GLS_Max_Principal_Stress__c='0.124508008',PIP_GLS_Max_Deflection__c='0.007759916'),
            new PIP_GLS_Plate_Calculator__c(Name='Test2',PIP_GLS_Interlayer_Material__C='SentryGlas®',PIP_GLS_Length_Range__c='500-1000',PIP_GLS_Length__c='600',PIP_GLS_Width_Range__c='500-1000',PIP_GLS_Width__c='600',PIP_GLS_Pressure_Range__c='0-2',PIP_GLS_Pressure__c='0.50',PIP_GLS_Glass_Thickness__c='6',PIP_GLS_Interlayer_Thickness__c='1.52',PIP_GLS_Temperature__c='50',PIP_GLS_Load_Duration__c='3',PIP_GLS_Max_Principal_Stress__c='exceeded',PIP_GLS_Max_Deflection__c='exceeded')
        };
        for(integer i=3;i<=250;i++){
            PIP_GLS_Plate_Calculator__c p=new PIP_GLS_Plate_Calculator__c(name='Test'+i,PIP_GLS_Interlayer_Material__C='SentryGlas®',PIP_GLS_Length_Range__c='500-1000',PIP_GLS_Length__c='500',PIP_GLS_Width_Range__c='500-1000',PIP_GLS_Width__c='500',PIP_GLS_Pressure_Range__c='0-2',PIP_GLS_Pressure__c='0.25',PIP_GLS_Glass_Thickness__c='6',PIP_GLS_Interlayer_Thickness__c='1.52',PIP_GLS_Temperature__c='50',PIP_GLS_Load_Duration__c='3',PIP_GLS_Max_Principal_Stress__c='0.124508008',PIP_GLS_Max_Deflection__c='0.007759916');
            PCK.add(p);
        }

       // UserRole r=[SELECT Id FROM UserRole WHERE Name='' limit 1]; 
        Profile p =[SELECT Id FROM Profile where name='System Administrator' limit 1]; 
        User u= new User(Firstname='TestUser1',Alias='tUs1', Email='S_R@test.com', 
                //User_Country__c='UNITED KINGDOM',EPass_ID__c='xxxxx',
                profileid = p.id,Username='User1@cts.com.devOps', LastName='Hill',
                CommunityNickname='testingPIPPlate', TimeZoneSidKey='America/Los_Angeles', LocaleSidKey='en_US',
                EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US');
        insert u;
        system.runAs(u){
            insert PCK;

            ctrlPIPGLS_PlateCalculator ctrlPC=new ctrlPIPGLS_PlateCalculator ();
            ctrlPC.pc=new PIP_GLS_Plate_Calculator__c(PIP_GLS_Interlayer_Material__C='SentryGlas®',PIP_GLS_Length_Range__c='500-1000',PIP_GLS_Length__c='500',PIP_GLS_Width_Range__c='500-1000',PIP_GLS_Width__c='500',PIP_GLS_Pressure_Range__c='0-2',PIP_GLS_Pressure__c='0.25',PIP_GLS_Glass_Thickness__c='6',PIP_GLS_Interlayer_Thickness__c='1.52',PIP_GLS_Temperature__c='50',PIP_GLS_Load_Duration__c='3');
            ctrlPC.search();
            ctrlPC.getPlateCalculator();
            ctrlPC.directMainPage();
            CtrlPC.saveFile();
            CtrlPC.closePopup();
            CtrlPC.showPopup();
            CtrlPC.samePage();
            CtrlPC.getImageURL();    
            CtrlPc.directHomePage();

            ctrlPIPGLS_PlateCalculator ctrlPC1=new ctrlPIPGLS_PlateCalculator ();
            ctrlPC1.pc=new PIP_GLS_Plate_Calculator__c(PIP_GLS_Interlayer_Material__C='SentryGlas®',PIP_GLS_Length_Range__c='500-1000',PIP_GLS_Length__c='600',PIP_GLS_Width_Range__c='500-1000',PIP_GLS_Width__c='600',PIP_GLS_Pressure_Range__c='0-2',PIP_GLS_Pressure__c='0.50',PIP_GLS_Glass_Thickness__c='6',PIP_GLS_Interlayer_Thickness__c='1.52',PIP_GLS_Temperature__c='50',PIP_GLS_Load_Duration__c='3');
            ctrlPC1.search(); 
        }
    }

    static testMethod void ctrlPIPGLS_PlateCalculator_Method2() {
        //UserRole r=[SELECT Id FROM UserRole WHERE Name='Corporate' limit 1]; 
        Profile p =[SELECT Id FROM Profile where name='System Administrator' limit 1]; 
        User u= new User(Firstname='TestUser2',Alias='tU2', Email='S_R@test.com', 
                //User_Country__c='UNITED KINGDOM',EPass_ID__c='xxxxx',
                profileid = p.id,Username='User1@cts.com.devOps', LastName='Hill',
                CommunityNickname='testingPIPPlate2', TimeZoneSidKey='America/Los_Angeles', LocaleSidKey='en_US',
                EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US');
        insert u;
        system.runAs(u){
            PIP_GLS_Plate_Calculator__c PCK=new PIP_GLS_Plate_Calculator__c(Name='Test1',PIP_GLS_Interlayer_Material__C='SentryGlas®' ,PIP_GLS_Length_Range__c='500-1000',PIP_GLS_Length__c='500',PIP_GLS_Width_Range__c='500-1000',PIP_GLS_Width__c='500',PIP_GLS_Pressure_Range__c='0-2',PIP_GLS_Pressure__c='0.25',PIP_GLS_Glass_Thickness__c='6',PIP_GLS_Interlayer_Thickness__c='1.52',  PIP_GLS_Temperature__c='50', PIP_GLS_Load_Duration__c='3',PIP_GLS_Max_Principal_Stress__c='0.124508008',PIP_GLS_Max_Deflection__c='0.007759916');
            insert PCK;

            ctrlPIPGLS_PlateCalculator ctrlPC=new ctrlPIPGLS_PlateCalculator ();
            ctrlPC.search();

            ctrlPIPGLS_PlateCalculator ctrlPC2=new ctrlPIPGLS_PlateCalculator ();
            ctrlPC2.pc=new PIP_GLS_Plate_Calculator__c(PIP_GLS_Interlayer_Material__C='SentryGlas®',PIP_GLS_Length_Range__c='500-1000',PIP_GLS_Length__c='700',PIP_GLS_Width_Range__c='500-1000',PIP_GLS_Width__c='400',PIP_GLS_Pressure_Range__c='0-2',PIP_GLS_Pressure__c='0.75',PIP_GLS_Glass_Thickness__c='6',PIP_GLS_Interlayer_Thickness__c='1.52',PIP_GLS_Temperature__c='50',PIP_GLS_Load_Duration__c='3');
            ctrlPC2.search();
        }
    }



}