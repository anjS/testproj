/***************************************************************************************
Copyright © 2014 . All rights reserved. 
Author: Anjali Singh
Email: Anjali_Singh04@infosys.com
Description:  Controller class for VF pages related to P&IP GLS Plate Calculator object. 
****************************************************************************************/
public class ctrlPIPGLS_PlateCalculator {
   

// Declaration of Class variables
    public PIP_GLS_Plate_Calculator__c pc {get; set;}
    List<PIP_GLS_Plate_Calculator__c> pcList= new List<PIP_GLS_Plate_Calculator__c>();
    List<PIP_GLS_Plate_Calculator__c> pcFormatedList= new List<PIP_GLS_Plate_Calculator__c>(); 
    public List<img> imageUrl;
    
 // flag : flag variables to decide the display of pdf page/popup/validation/error messages on page
    public boolean displayPopUpFlag {get; set;}
    public boolean validationFlag{get; set;}    
    public boolean errFlag{get; set;} 
    public boolean errMsgFlag{get; set;} 
    public boolean saveFlag{get; set;} 
    public String renderAsFlag {get; set;}
    public boolean displayPdfPage{get; set;} 
    public boolean displayMainPage{get; set;} 
  
    
   
   
   /**
    * Name: img
    * Params: None
    * Description: Class to bind images for displaying on pages
    */
    public class img{
       public String imageName{get; set;}
       public String imageURL{get; set;}
       public String imageValuesURL{get; set;}
       public String staticResourceName{get; set;}
   }
   
  /**
    * Name: ctrlPlateCalculator
    * Params: None
    * Description: Constructor for the class
    */ 
    public ctrlPIPGLS_PlateCalculator () {
      pc=new PIP_GLS_Plate_Calculator__c();
      errMsgFlag=false;
      renderAsFlag ='';
      saveFlag=false;
      displayPdfPage=false;
      displayMainPage=false;
          
    }
    
   /**
    * Name: search
    * Params: None
    * Description: Method to search a particular record in the database
    */   
    public PageReference search(){
    try{
        validationFlag=false;
        errFlag= false;
        saveFlag=true;
        errMsgFlag = false;
        pcFormatedList=new List<PIP_GLS_Plate_Calculator__c>();
        system.debug('-----len--------'+pc.PIP_GLS_Length_Range__C +'and' + pc.PIP_GLS_Length__c);
          system.debug('------wid-------'+pc.PIP_GLS_Width_Range__C +'and' + pc.PIP_GLS_Width__c);
            system.debug('------press-------'+pc.PIP_GLS_Pressure_Range__C +'and' + pc.PIP_GLS_Pressure__c);
        validation();
        
        if(validationFlag==false){                
                String lwCombination= 'A'+ (Integer.valueOf((pc.PIP_GLS_Length__c)) *  Integer.valueOf((pc.PIP_GLS_Width__c))) +'P'
                                 + (2 * Integer.valueOf((pc.PIP_GLS_Length__c)) + 2 * Integer.valueOf((pc.PIP_GLS_Width__c)));                                 
                String inputString = lwCombination +'-'+pc.PIP_GLS_Glass_thickness__c 
                                     +'-'+pc.PIP_GLS_Interlayer_Thickness__c+'-'+pc.PIP_GLS_Interlayer_Material__c+'-'+pc.PIP_GLS_Temperature__c
                                     +'-'+pc.PIP_GLS_Load_Duration__c+'-'+pc.PIP_GLS_Pressure__c; 
                String md5Coded = EncodingUtil.convertToHex(Crypto.generateDigest('MD5', Blob.valueOf(inputString)));
                       
                      
                pcList=[Select PIP_GLS_Plate_Calculator_Key__c,Id,PIP_GLS_Max_Deflection__c,PIP_GLS_Max_Principal_Stress__c FROM PIP_GLS_Plate_Calculator__c WHERE PIP_GLS_Plate_Calculator_Key__c =:md5Coded ];
                     
                      if(pcList.size()==0) {
                          errMsgFlag = true; 
                          pcFormatedList=pcList;  
                      }else {                                       
                          for (PIP_GLS_Plate_Calculator__c pc :pcList) {
                          if((pc.PIP_GLS_Max_Deflection__c).equals('exceeded') &&  (pc.PIP_GLS_Max_Principal_Stress__c).equals('exceeded')){
                              pc.PIP_GLS_Max_Deflection__c=PIPGLS_PlateCalculator_Setting__c.getInstance('Deflection Values').exceededValue__c;
                              pc.PIP_GLS_Max_Principal_Stress__c =PIPGLS_PlateCalculator_Setting__c.getInstance('Stress Values').exceededValue__c;
                          }
                          else {
                          
                           pc.PIP_GLS_Max_Deflection__c= (Decimal.valueOf(pc.PIP_GLS_Max_Deflection__c).setScale(2)).format();
                           pc.PIP_GLS_Max_Principal_Stress__c = (Decimal.valueOf(pc.PIP_GLS_Max_Principal_Stress__c).setScale(2)).format();

                             }
                              pcFormatedList.add(pc);
                             } 
                            } 
                         }
        }
        catch(Exception e){ }  
        return null; 
        }
    
   /**
    * Name: getPlateCalculator
    * Params: None
    * Description: Method to return list of record based on search criteria to visualforce page
    */ 
    
    public List<PIP_GLS_Plate_Calculator__c> getPlateCalculator() { 
        return  pcFormatedList;
        } 
  
 /**
    * Name: getImageUrl
    * Params: None
    * Description: Method to fetch images for the particular record and return the list of images.
    */
    
    public List<img> getImageUrl(){    
        
        imageUrl=new List<img>();
        try {         
             
             String lwCombination= 'A'+ (Integer.valueOf((pc.PIP_GLS_Length__c)) *  Integer.valueOf((pc.PIP_GLS_Width__c))) +'P'
                                 + (2 * Integer.valueOf((pc.PIP_GLS_Length__c)) + 2 * Integer.valueOf((pc.PIP_GLS_Width__c)));   
                                 
             String imgDeflection_url = PIPGLS_PlateCalculator_Setting__c.getInstance('Deflection Values'). imageUrl__c;
             String imgStress_url = PIPGLS_PlateCalculator_Setting__c.getInstance('Stress Values'). imageUrl__c;
             String imgDeflection_name =PIPGLS_PlateCalculator_Setting__c.getInstance('Deflection Values').imageName__c;
             String imgStress_name = PIPGLS_PlateCalculator_Setting__c.getInstance('Stress Values').imageName__c;
             String imgDeflection_extension = PIPGLS_PlateCalculator_Setting__c.getInstance('Deflection Values').imageExtension__c;
             String imgStress_extension = PIPGLS_PlateCalculator_Setting__c.getInstance('Stress Values').imageExtension__c;
             
                                                               
             for(StaticResource sr:[SELECT Name, NamespacePrefix, SystemModStamp,Description from StaticResource
                                      where Description like '%PlateCalculator%']) {                                
                    Integer iFlag=0; 
                    String strName=null;  
                
                        if(iFlag==0 || strName=='Image Exists') {
                                 
                            try {
                                     new PageReference('/resource/' + sr.Name + '/' + sr.Name + imgDeflection_url + lwCombination + imgDeflection_extension).getContent();
                                     strName='Image Exists';                 
                                     img imgDeflection= new img();
                                     imgDeflection.imageName= imgDeflection_name;
                                     imgDeflection.imageURL= '/'+sr.Name+imgDeflection_url+lwCombination+imgDeflection_extension;
                                     imgDeflection.imageValuesURL='PIPGLS_PlateCalculator_HeaderImages/PIPGLS_PlateCalculator_Deflection_help_image.jpg';
                                     imgDeflection.staticResourceName= sr.Name;
                                     imageUrl.add(imgDeflection);
                                     system.debug('--Deflection---------'+ imgDeflection.imageValuesURL);
                                     
                                }
                                 catch(Exception e){
                                       iFlag++;  
                                                                                         
                                     }
                    
                        if(iFlag!=0 || strName=='Image Exists') {
                             try {
                                   new PageReference('/resource/' + sr.Name + '/' + sr.Name + imgStress_url + lwCombination + imgStress_extension).getContent();
                                   strName='Image Exists';               
                                   img imgStress= new img();
                                   imgStress.imageName= imgStress_name;
                                   imgStress.imageURL= '/'+sr.Name+imgStress_url+lwCombination+imgStress_extension;
                                   imgStress.staticResourceName= sr.Name;
                                   imgStress.imageValuesURL='PIPGLS_PlateCalculator_HeaderImages/PIPGLS_PlateCalculator_Stress_help_image.jpg';
                                   imageUrl.add(imgStress);  
                               
                                                                              
                             }
                             catch(Exception e){
                                     iFlag=0;
                                                                         
                                   }
                            }
                        }
                   }
             
            }
            
            catch(Exception e){}
          return imageUrl;
       }
    
  /**
    * Name: saveFile
    * Params: None
    * Description: Method to display single record as pdf
    */
    
    public pageReference saveFile() {
        renderAsFlag = 'PDF';
        saveFlag=false;
        displayPdfPage=true;
        PageReference pageRef= new PageReference('/apex/PIPGLS_PlateCalculatorPage');
        pageRef.setRedirect(false);
        return pageRef;
        
        }
    
  /**
    * Name: validation
    * Params: None
    * Description: Method to check the validation for all input fields
    */   
    
    public void validation(){
        try {
                if(pc.PIP_GLS_Length_Range__c != Null){
              String length=  pc.PIP_GLS_Length_Range__c;
                 pc.PIP_GLS_Length_Range__c = length;
                 validationFlag=false;
                errFlag= false;
            system.debug('------validation-len------'+pc.PIP_GLS_Length_Range__C +'and' + pc.PIP_GLS_Length__c);
             system.debug('------validation--len-----'+length);
                system.debug('----vali--wid-------'+pc.PIP_GLS_Width_Range__C +'and' + pc.PIP_GLS_Width__c);
            system.debug('-----vali-press-------'+pc.PIP_GLS_Pressure_Range__C +'and' + pc.PIP_GLS_Pressure__c);
                }
            
            if(pc.PIP_GLS_Length_Range__c == Null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please select Length Range'));
                validationFlag=true;
                errFlag= true;
                }
                
            if(pc.PIP_GLS_Length__c == Null || pc.PIP_GLS_Length__c == '--Please Select--'){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please select Length '));
                validationFlag=true;
                errFlag= true;
                }
            
            if(pc.PIP_GLS_Width_Range__c == Null  || pc.PIP_GLS_Width_Range__c=='PlSelectWidMs'){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please select width Range'));
                validationFlag=true;
                errFlag= true;
                }
            
            if(pc.PIP_GLS_Width__c == Null  || pc.PIP_GLS_Width__c == 'PlSelectWidCh'){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please select width'));
                validationFlag=true;
                errFlag= true;
                }
            
            if(pc.PIP_GLS_Glass_Thickness__c == Null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.FATAL,'Please select Glass Thickness'));
                validationFlag=true;
                errFlag= true;
                }
            
            if(pc.PIP_GLS_Interlayer_Thickness__c == Null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please select Interlayer Thickness'));
                validationFlag=true;
                errFlag= true;
                }
            
            if(pc.PIP_GLS_Pressure_Range__c == Null || pc.PIP_GLS_Pressure_Range__c=='PlSelectPrMs'){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please select Pressure Range'));
                validationFlag=true;
                errFlag= true;
                }
            
            if(pc.PIP_GLS_Pressure__c == Null  || pc.PIP_GLS_Pressure__c == 'PlSelectPrCh'){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please select Pressure'));
                validationFlag=true;
                errFlag= true;
                }    
            
            if(pc.PIP_GLS_Interlayer_Material__c == Null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please select Interlayer Material'));
                validationFlag=true;
                errFlag= true;
                }
            
            if(pc.PIP_GLS_Temperature__c == Null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.FATAL,'Please select Temperature'));
                              
                validationFlag=true;
                errFlag= true;
                }
            
            if(pc.PIP_GLS_Load_Duration__c == Null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please select Load Duration'));
                validationFlag=true;
                errFlag= true;
                }  
            }
            
            catch(Exception e){ }   
            }
   
 /* Follwoing methods are used in Disclaimer page for applying pop up logic and going to specific page on button click */  
    /**
    * Name: closePopup
    * Params: None
    * Description: Method to set flag variable value to false to stop displaying pop up in disclaimer page
    */
    
    public void closePopup() {        
        displayPopUpFlag = false;    
        } 
        
    /**
    * Name: showPopup
    * Params: None
    * Description: Method to set variable value to true to display pop up in disclaimer page
    */
    
    public void showPopUp() {        
        displayPopUpFlag = true;    
        displayMainPage=false;
        }
    
    /**
    * Name: samePage
    * Params: None
    * Description: Method to set variable value to false to stop displaying pop up in disclaimer page and be on the same page
    */  
    
    public pageReference samePage(){
        displayPopUpFlag = false; 
        return null;
        }
    
    
      /**
    * Name: directHomePage
    * Params: None
    * Description: Method to check the url of the current and accordingly direct the user to specific home page.
    */
     public PageReference directHomePage() {
        PageReference pageRef;
        String pageUrl = URL.getCurrentRequestUrl().toExternalForm();
        String hostUrl=System.URL.getSalesforceBaseUrl().toExternalForm();
          if(pageUrl.startsWith(hostUrl)){
             String stringName = pageUrl .substring(hostUrl.length(),
                    pageUrl.length());
             if(stringName.contains('/Sentry')){
                     pageRef= new PageReference(hostUrl +'/SentryGlasClub/home/home.jsp');
               }
             else {
                 pageRef= new PageReference(hostUrl + '/home/home.jsp');
               } 
             }
             
       return pageRef;
    }
 /**
    * Name: directMainPage
    * Params: None
    * Description: Method to set flag variables value to decide the page block rendering and to set default values
    */
     public void directMainPage(){
       displayMainPage = true; 
       displayPdfPage=false;
       renderAsFlag='';
       saveFlag =false;  
       pc.PIP_GLS_Interlayer_Material__c='SentryGlas®';
       pc.PIP_GLS_Temperature__c='50';
       pc.PIP_GLS_Load_Duration__c='3';
      
       
    }

}