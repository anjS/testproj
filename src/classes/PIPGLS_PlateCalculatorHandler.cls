/*******************************************************************************
Copyright © 2014 DuPont. All rights reserved. 
Author: Anjali Singh
Email: Anjali_Singh04@infosys.com
Description:  Trigger Handler class for P&IP GLS Plate Calculator Object. 
********************************************************************************/
public class PIPGLS_PlateCalculatorHandler { // extends TriggerHandlerBase{
//public override void bulkBefore(){
     //   if(trigger.isInsert || trigger.IsUpdate){
       //     plateCalculate();
        //}
  //  }
        
    public void plateCalculate(){    
        for(Sobject sobj:trigger.new){
           PIP_GLS_Plate_Calculator__c pc=(PIP_GLS_Plate_Calculator__c)sobj; 
            if(pc.PIP_GLS_Length__c.length()!=0&&pc.PIP_GLS_Width__c.length()!=0&&pc.PIP_GLS_Glass_thickness__C.length()!=0&& 
                                     pc.PIP_GLS_Interlayer_Thickness__c.length()!=0&&pc.PIP_GLS_Interlayer_Material__c.length()!=0
                                     &&pc.PIP_GLS_Temperature__c.length()!=0&&
                                     pc.PIP_GLS_Load_Duration__c.length()!=0&&pc.PIP_GLS_Pressure__c.length()!=0){   
            String lwCombination= 'A'+ (Integer.valueOf((pc.PIP_GLS_Length__c)) *  Integer.valueOf((pc.PIP_GLS_Width__c))) +'P'
                                 + (2 * Integer.valueOf((pc.PIP_GLS_Length__c)) + 2 * Integer.valueOf((pc.PIP_GLS_Width__c)));
                                 
            String inputString = lwCombination +'-'+pc.PIP_GLS_Glass_thickness__C 
                                     +'-'+pc.PIP_GLS_Interlayer_Thickness__c+'-'+pc.PIP_GLS_Interlayer_Material__c+'-'+pc.PIP_GLS_Temperature__c
                                     +'-'+pc.PIP_GLS_Load_Duration__c+'-'+pc.PIP_GLS_Pressure__c;                                
           
            String md5Coded = EncodingUtil.convertToHex(Crypto.generateDigest('MD5', Blob.valueOf(inputString)));
            pc.PIP_GLS_Plate_Calculator_Key__c=md5Coded;  
            }
        } 
      
   }

   

}